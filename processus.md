# Processus de création

Le client voulait une landing page orienté sur la photographie d'architecture.
Il voulait quelque chose de blanc, simple et ne cherchait pas un site avec trop de couleurs.

## Structure
La structure du site a été faite sur du **Bootstrap**.
Comme ça le client pourras le modifier à tout moments sans difficultés.

Il y a également du:
- **Javascript**
- **jQuery**

Ces deux **languages** sont utilisés pour les diaporamas présents sur le site.


**À finir !**
